package vais.product.libexample.model

data class CustomModel(val id: Int, val content: String)