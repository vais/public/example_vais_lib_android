package vais.product.libexample
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import vais.product.voice_recognition.VaisRecorder


class MainActivity: AppCompatActivity(){
    private var vaisRecorder = VaisRecorder.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        vaisRecorder.initModule(this)
        btnVerify.setOnClickListener{
            val intent = Intent (this, RecordVerifyActivity::class.java)
            startActivity(intent)
        }

        btnGenkey.setOnClickListener{
            val intent = Intent (this, RecordGenKeyActivity::class.java)
            startActivity(intent)
        }


        btnGenKeyOff.setOnClickListener{
            val intent = Intent (this, RecordGenKeyOff::class.java)
            startActivity(intent)
        }


    }


}