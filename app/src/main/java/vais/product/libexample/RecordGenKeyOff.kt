package vais.product.libexample

import android.Manifest
import android.content.ClipboardManager
import android.content.Context
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_record_gen_key_off.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import vais.product.libexample.adapter.CustomAdapter
import vais.product.libexample.model.CustomModel
import vais.product.voice_recognition.VaisRecorder
import vais.product.voice_recognition.model.GenkeyState
import vais.product.voice_recognition.model.ModeState
import vais.product.voice_recognition.model.RecorderState
import vais.product.voice_recognition.model.ValueVerify
import java.io.*
import java.util.*
import java.util.concurrent.TimeUnit

class RecordGenKeyOff : AppCompatActivity() {

    private val PERMISSIONS_REQUEST_RECORD_AUDIO = 77

    private var vaisRecorder = VaisRecorder.getInstance()
    private lateinit var filePath: String
    private var isRecording = false
    private var isPaused = false
    private var totalAudioRecord = 0
    var keyGen: ByteArray? = null

    private var dataList = mutableListOf<CustomModel>(

    )

    private var customAdapter = CustomAdapter() { position: Int ->
        deleteCustomModel(position)
    }

    private fun deleteCustomModel(position: Int) {
        dataList.removeAt(position)
        customAdapter.notifyItemRemoved(position)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_record_gen_key_off)

        try {
            vaisRecorder.changeRecordLength(2)
        } catch (
            e: Exception
        ) {
            Toast.makeText(this, e.message, Toast.LENGTH_SHORT).show()
        }
        customAdapter.submitList(dataList)
        lstAudioUserOff.apply {
            layoutManager = LinearLayoutManager(this@RecordGenKeyOff)
            adapter = customAdapter
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        }


        filePath = externalCacheDir?.absolutePath + "/audio_genkey_${totalAudioRecord + 1}.wav"

        vaisRecorder.changeFilePath(filePath)
        vaisRecorder.changeMode(ModeState.OFFLINE)

        vaisRecorder.onStateChangeListener = {
            when (it) {
                RecorderState.RECORDING -> startRecording()
                RecorderState.STOP -> stopRecording()
                RecorderState.PAUSE -> pauseRecording()
            }
        }


        vaisRecorder.onGenKey = {
            Log.d("vaisdebug", it.genkeyState.name)

            llProgressBar3.visibility =
                if (it.genkeyState.name == GenkeyState.Loading.name) View.VISIBLE else View.GONE

            if (it.genkeyState.name == ValueVerify.SUCCESS.name) {
                messageTextViewOff.visibility = View.GONE
                btnGenKeyOff.visibility = View.GONE
                startStopRecordingButtonOff.visibility = View.GONE
                layoutGenKeyOff.visibility = View.VISIBLE

                txtKeyGenOff.text = it.value?.toString()
                keyGen = it.value!!


                dataList.clear();
                customAdapter.submitList(dataList)
                customAdapter.notifyDataSetChanged()

                Toast.makeText(this, "Success. Copy your key", Toast.LENGTH_SHORT).show()


            }
            else if (it.genkeyState.name == ValueVerify.FAIL.name) {
                Log.d("vaisdebug", "e2242")
                btnGenKeyOff.visibility = View.VISIBLE
                startStopRecordingButtonOff.visibility = View.VISIBLE
                Toast.makeText(this, it.error, Toast.LENGTH_SHORT).show()
            }

        }

        vaisRecorder.onTimeElapsed = {
            Log.e(RecordGenKeyOff.TAG, "onCreate: time elapsed $it")
            timeTextViewOff.text = formatTimeUnit(it * 1000)
        }


        startStopRecordingButtonOff.setOnClickListener {

            if (!isRecording) {
                if (ContextCompat.checkSelfPermission(
                        this,
                        Manifest.permission.RECORD_AUDIO
                    )
                    != PackageManager.PERMISSION_GRANTED
                ) {
                    ActivityCompat.requestPermissions(
                        this,
                        arrayOf(Manifest.permission.RECORD_AUDIO),
                        PERMISSIONS_REQUEST_RECORD_AUDIO
                    )
                } else {
                    filePath =
                        externalCacheDir?.absolutePath + "/audio_genkey_${totalAudioRecord + 1}.wav"
                    vaisRecorder.changeFilePath(filePath)
                    try{
                        vaisRecorder.startRecording(this)
                    }catch (e: Exception) {
                        Toast.makeText(this, e.message, Toast.LENGTH_SHORT).show()
                    }

                }
            } else {
                vaisRecorder.stopRecording()
            }
        }
        val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        txtCopyOff.setOnClickListener {

            writeBytesAsPdf(keyGen!!)
            Toast.makeText(this, "Copy Done", Toast.LENGTH_SHORT).show()

        }


        pauseResumeRecordingButtonOff.setOnClickListener {
            if (!isPaused) {
                vaisRecorder.pauseRecording()
            } else {
                vaisRecorder.resumeRecording()
            }
        }

        noiseSuppressorSwitchOff.setOnCheckedChangeListener { buttonView, isChecked ->
            vaisRecorder.noiseSuppressorActive = isChecked
            if (isChecked)
                Toast.makeText(this, "Noise Suppressor Activated", Toast.LENGTH_SHORT).show()

        }
        btnGenKeyOff.setOnClickListener {
            var listPath = arrayListOf<String>()
            dataList.forEach {
                listPath.add(it.content)
            }
            vaisRecorder.genKeyOffline( this, listPath)
        }
    }

    private fun startRecording() {
        Log.d(RecordGenKeyOff.TAG, vaisRecorder.audioSessionId.toString())
        Log.d("kiemtra","123342");
        isRecording = true
        isPaused = false
        messageTextViewOff.visibility = View.GONE
        lstAudioUserOff.visibility = View.GONE
        recordingTextViewOff.text = "Recording..."
        recordingTextViewOff.visibility = View.VISIBLE
        startStopRecordingButtonOff.text = "Waiting"
        startStopRecordingButtonOff.isEnabled = false
        btnGenKeyOff.visibility = View.GONE
        pauseResumeRecordingButtonOff.text = "PAUSE"
        pauseResumeRecordingButtonOff.visibility = View.GONE
        noiseSuppressorSwitchOff.isEnabled = false
    }

    private fun stopRecording() {
        isRecording = false
        isPaused = false
        totalAudioRecord += 1;

        recordingTextViewOff.visibility = View.GONE
        messageTextViewOff.visibility = View.VISIBLE
        lstAudioUserOff.visibility = View.VISIBLE
        pauseResumeRecordingButtonOff.visibility = View.GONE
        var newModel = CustomModel(totalAudioRecord, filePath)
        dataList.add(newModel);
        customAdapter.submitList(dataList);
        Toast.makeText(this, "File saved at : $filePath", Toast.LENGTH_LONG).show()
        startStopRecordingButtonOff.text = "START"
        startStopRecordingButtonOff.isEnabled = true
        noiseSuppressorSwitchOff.isEnabled = true
        GlobalScope.launch(Dispatchers.Main) {
            timeTextViewOff.text = ""
        }
        btnGenKeyOff.text = "Genkey Off"

        btnGenKeyOff.visibility = View.VISIBLE

    }

    fun writeBytesAsPdf(bytes: ByteArray) {
        var myExternalFile = File(getExternalFilesDir("MyFileStorage"), "abc.txt")
        try {
            val fileOutPutStream = FileOutputStream(myExternalFile)
            fileOutPutStream.write(bytes)
            fileOutPutStream.close()


            var myExternalFile = File(getExternalFilesDir("MyFileStorage"), "abc.txt")

            val filename = "abc.txt"
            myExternalFile = File(getExternalFilesDir("MyFileStorage"), filename)
            if (filename.toString() != null && filename.toString().trim() != "") {
                var fileInputStream = FileInputStream(myExternalFile)
                var inputStreamReader: InputStreamReader = InputStreamReader(fileInputStream)
                val bufferedReader: BufferedReader = BufferedReader(inputStreamReader)
                val stringBuilder: StringBuilder = StringBuilder()
                var text: String? = null
//                while ({ text = bufferedReader.readLine(); text }() != null) {
//                    stringBuilder.append(text)
//                }
                var adds = fileInputStream.readBytes()
                Log.d("kiemtra", adds.toString())
                Log.d("kiemtra", bytes.toString())

                Log.d("kiemtra", adds.contentEquals(bytes).toString())
                fileInputStream.close()
                //Displaying data on EditText
//                textInputEditText.setText(adds.toString())
//                Toast.makeText(applicationContext,stringBuilder.toString(),Toast.LENGTH_SHORT).show()
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        vaisRecorder.stopRecording()
    }

    private fun pauseRecording() {
        recordingTextViewOff.text = "PAUSE"
        pauseResumeRecordingButtonOff.text = "RESUME"
        isPaused = true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSIONS_REQUEST_RECORD_AUDIO -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    filePath =
                        externalCacheDir?.absolutePath + "/audio_genkey_${totalAudioRecord + 1}.wav"
                    vaisRecorder.changeFilePath(filePath)
                    try{
                        vaisRecorder.startRecording(this)
                    }catch (e: Exception) {
                        Toast.makeText(this, e.message, Toast.LENGTH_SHORT).show()
                    }
                }
                return
            }

            else -> {
            }
        }
    }

    companion object {
        private const val TAG = "MainActivity"
    }

    private fun formatTimeUnit(timeInMilliseconds: Long): String {
        return try {
            String.format(
                Locale.getDefault(),
                "%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(timeInMilliseconds),
                TimeUnit.MILLISECONDS.toSeconds(timeInMilliseconds) - TimeUnit.MINUTES.toSeconds(
                    TimeUnit.MILLISECONDS.toMinutes(timeInMilliseconds)
                )
            )
        } catch (e: Exception) {
            "00:00"
        }
    }
}