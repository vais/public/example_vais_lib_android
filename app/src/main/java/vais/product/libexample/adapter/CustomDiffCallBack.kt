package vais.product.libexample.adapter

import androidx.recyclerview.widget.DiffUtil
import vais.product.libexample.model.CustomModel

class CustomDiffCallBack : DiffUtil.ItemCallback<CustomModel>() {

    override fun areItemsTheSame(oldItem: CustomModel, newItem: CustomModel): Boolean {
        return oldItem?.id == newItem?.id

    }

    override fun areContentsTheSame(oldItem: CustomModel, newItem: CustomModel): Boolean {
        return oldItem == newItem
    }

}